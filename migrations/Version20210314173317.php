<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314173317 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ilceler (id INT AUTO_INCREMENT NOT NULL, iller_id INT NOT NULL, ilce_adi VARCHAR(60) NOT NULL, nufus INT NOT NULL, erkek_nufus INT NOT NULL, kadin_nufus INT NOT NULL, yuzolcumu INT NOT NULL, INDEX IDX_C952AE7390420141 (iller_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE iller (id INT AUTO_INCREMENT NOT NULL, il_adi VARCHAR(60) NOT NULL, plaka_kodu INT NOT NULL, alan_kodu INT NOT NULL, nufus INT NOT NULL, bolge VARCHAR(90) NOT NULL, yuzolcumu VARCHAR(60) NOT NULL, erkek_nufus INT NOT NULL, kadin_nufus INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ilceler ADD CONSTRAINT FK_C952AE7390420141 FOREIGN KEY (iller_id) REFERENCES iller (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ilceler DROP FOREIGN KEY FK_C952AE7390420141');
        $this->addSql('DROP TABLE ilceler');
        $this->addSql('DROP TABLE iller');
    }
}
