<?php

namespace App\Repository;

use App\Entity\Iller;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Iller|null find($id, $lockMode = null, $lockVersion = null)
 * @method Iller|null findOneBy(array $criteria, array $orderBy = null)
 * @method Iller[]    findAll()
 * @method Iller[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IllerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Iller::class);
    }


    // /**
    //  * @return Iller[] Returns an array of Iller objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Iller
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
