<?php

namespace App\Repository;

use App\Entity\Ilceler;
use App\Entity\Iller;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ilceler|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ilceler|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ilceler[]    findAll()
 * @method Ilceler[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IlcelerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ilceler::class);
    }


    // /**
    //  * @return Ilceler[] Returns an array of Ilceler objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ilceler
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
