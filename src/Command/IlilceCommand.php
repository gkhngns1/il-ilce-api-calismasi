<?php

namespace App\Command;

use App\Entity\Ilceler;
use App\Entity\Iller;
use App\Service\ApiServis;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class IlilceCommand extends Command
{
    public $entityManager;
    protected static $defaultName = 'ililce';
    protected static $defaultDescription = 'Add a short description for your command';
    public function __construct(EntityManagerInterface $entty)
    {
        $this->entityManager = $entty;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('isim', null, InputOption::VALUE_OPTIONAL, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        $cevap = $io->ask("Tablolar temizlenecek emin misin ? ","yes");
        if($cevap=="no"){
            return true;
        }
        $iller = $this->entityManager->getRepository(Iller::class)->findAll();
        foreach ($iller as $il){
            $this->entityManager->remove($il);
            $this->entityManager->flush();
        }
        $client = new ApiServis("https://raw.githubusercontent.com/snrylmz/il-ilce-json/master/js/il-ilce.json");
        $array = json_decode($client->body);
        $io->progressStart(count($array->data));
        foreach ($array->data as $index => $ilData){
            $il = new Iller();
            $il->setIlAdi($ilData->il_adi);
            $il->setPlakaKodu(intval(str_replace(".","",$ilData->plaka_kodu)));
            $il->setAlanKodu(intval(str_replace(".","",$ilData->alan_kodu)));
            $il->setBolge($ilData->bolge);
            $il->setNufus(intval(str_replace(".","",$ilData->nufus)));
            $il->setYuzolcumu(intval(str_replace(".","",$ilData->yuzolcumu)));
            $il->setErkekNufus(intval(str_replace(".","",$ilData->erkek_nufus)));
            $il->setKadinNufus(intval(str_replace(".","",$ilData->kadin_nufus)));
            $this->entityManager->persist($il);
            $this->entityManager->flush();
            foreach ($ilData->ilceler as $ilceData){
                $ilce = new Ilceler();
                $ilce->setIlceAdi($ilceData->ilce_adi);
                $ilce->setNufus(intval(str_replace(".","",$ilceData->nufus)));
                $ilce->setKadinNufus(intval(str_replace(".","",$ilceData->kadin_nufus)));
                $ilce->setErkekNufus(intval(str_replace(".","",$ilceData->erkek_nufus)));
                $ilce->setYuzolcumu(intval(str_replace(".","",$ilceData->yuzolcumu)));
                $ilce->setIller($il);
                $this->entityManager->persist($ilce);
                $this->entityManager->flush();

            }
            $io->progressAdvance(1);

        }
        $io->progressFinish();
        $io->success("Tebrikler Tamamlandi");

        return Command::SUCCESS;
    }
}
