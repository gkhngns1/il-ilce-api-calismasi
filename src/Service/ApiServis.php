<?php

namespace App\Service;

class  ApiServis{
    public $url;
    public $body;
    public function __construct(string $url){
        $this->url = $url;
        $client = new \GuzzleHttp\Client();
        $request = $client->request("GET",$this->url);
        $this->body = $request->getBody();

    }


}