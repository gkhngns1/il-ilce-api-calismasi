<?php

namespace App\Entity;

use App\Repository\IlcelerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IlcelerRepository::class)
 */
class Ilceler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $ilce_adi;

    /**
     * @ORM\Column(type="integer")
     */
    private $nufus;

    /**
     * @ORM\Column(type="integer")
     */
    private $erkek_nufus;

    /**
     * @ORM\Column(type="integer")
     */
    private $kadin_nufus;

    /**
     * @ORM\Column(type="integer")
     */
    private $yuzolcumu;

    /**
     * @ORM\ManyToOne(targetEntity=Iller::class, inversedBy="ilceler")
     * @ORM\JoinColumn(nullable=false ,onDelete="CASCADE")
     */
    private $iller;


    public function __construct()
    {
        $this->il = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIlceAdi(): ?string
    {
        return $this->ilce_adi;
    }

    public function setIlceAdi(string $ilce_adi): self
    {
        $this->ilce_adi = $ilce_adi;

        return $this;
    }

    public function getNufus(): ?int
    {
        return $this->nufus;
    }

    public function setNufus(int $nufus): self
    {
        $this->nufus = $nufus;

        return $this;
    }

    public function getErkekNufus(): ?int
    {
        return $this->erkek_nufus;
    }

    public function setErkekNufus(int $erkek_nufus): self
    {
        $this->erkek_nufus = $erkek_nufus;

        return $this;
    }

    public function getKadinNufus(): ?int
    {
        return $this->kadin_nufus;
    }

    public function setKadinNufus(int $kadin_nufus): self
    {
        $this->kadin_nufus = $kadin_nufus;

        return $this;
    }

    public function getYuzolcumu(): ?int
    {
        return $this->yuzolcumu;
    }

    public function setYuzolcumu(int $yuzolcumu): self
    {
        $this->yuzolcumu = $yuzolcumu;

        return $this;
    }

    public function getIller(): ?Iller
    {
        return $this->iller;
    }

    public function setIller(?Iller $iller): self
    {
        $this->iller = $iller;

        return $this;
    }


}
