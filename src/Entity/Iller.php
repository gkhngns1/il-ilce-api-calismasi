<?php

namespace App\Entity;

use App\Repository\IllerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IllerRepository::class)
 */
class Iller
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $il_adi;

    /**
     * @ORM\Column(type="integer")
     */
    private $plaka_kodu;

    /**
     * @ORM\Column(type="integer")
     */
    private $alan_kodu;

    /**
     * @ORM\Column(type="integer")
     */
    private $nufus;

    /**
     * @ORM\Column(type="string", length=90)
     */
    private $bolge;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $yuzolcumu;

    /**
     * @ORM\Column(type="integer")
     */
    private $erkek_nufus;

    /**
     * @ORM\Column(type="integer")
     */
    private $kadin_nufus;

    /**
     * @ORM\OneToMany(targetEntity=Ilceler::class, mappedBy="iller")
     */
    private $ilceler;

    public function __construct()
    {
        $this->ilceler = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIlAdi(): ?string
    {
        return $this->il_adi;
    }

    public function setIlAdi(string $il_adi): self
    {
        $this->il_adi = $il_adi;

        return $this;
    }

    public function getPlakaKodu(): ?int
    {
        return $this->plaka_kodu;
    }

    public function setPlakaKodu(int $plaka_kodu): self
    {
        $this->plaka_kodu = $plaka_kodu;

        return $this;
    }

    public function getAlanKodu(): ?int
    {
        return $this->alan_kodu;
    }

    public function setAlanKodu(int $alan_kodu): self
    {
        $this->alan_kodu = $alan_kodu;

        return $this;
    }

    public function getNufus(): ?int
    {
        return $this->nufus;
    }

    public function setNufus(int $nufus): self
    {
        $this->nufus = $nufus;

        return $this;
    }

    public function getBolge(): ?string
    {
        return $this->bolge;
    }

    public function setBolge(string $bolge): self
    {
        $this->bolge = $bolge;

        return $this;
    }

    public function getYuzolcumu(): ?string
    {
        return $this->yuzolcumu;
    }

    public function setYuzolcumu(string $yuzolcumu): self
    {
        $this->yuzolcumu = $yuzolcumu;

        return $this;
    }

    public function getErkekNufus(): ?int
    {
        return $this->erkek_nufus;
    }

    public function setErkekNufus(int $erkek_nufus): self
    {
        $this->erkek_nufus = $erkek_nufus;

        return $this;
    }

    public function getKadinNufus(): ?int
    {
        return $this->kadin_nufus;
    }

    public function setKadinNufus(int $kadin_nufus): self
    {
        $this->kadin_nufus = $kadin_nufus;

        return $this;
    }

    /**
     * @return Collection|ilceler[]
     */
    public function getIlceler(): Collection
    {
        return $this->ilceler;
    }

    public function addIlceler(ilceler $ilceler): self
    {
        if (!$this->ilceler->contains($ilceler)) {
            $this->ilceler[] = $ilceler;
            $ilceler->setIller($this);
        }

        return $this;
    }

    public function removeIlceler(ilceler $ilceler): self
    {
        if ($this->ilceler->removeElement($ilceler)) {
            // set the owning side to null (unless already changed)
            if ($ilceler->getIller() === $this) {
                $ilceler->setIller(null);
            }
        }

        return $this;
    }
}
