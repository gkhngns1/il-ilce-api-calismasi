<?php

namespace App\Controller;

use App\Entity\Ilceler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IlceController extends AbstractController
{
    /**
     * @Route("/ilce/{ilce_id}", name="ilce")
     */
    public function index($ilce_id): Response
    {
        $ilce = $this->getDoctrine()->getRepository(Ilceler::class)->find($ilce_id);
        return $this->render('ilce/index.html.twig', [
            'controller_name' => 'IlceController',
            "ilce"=>$ilce
        ]);
    }
}
