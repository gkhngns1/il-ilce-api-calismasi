<?php

namespace App\Controller;

use App\Entity\Iller;
use App\Service\ApiServis;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnasayfaController extends AbstractController
{
    /**
     * @Route("/", name="anasayfa")
     */
    public function index(): Response
    {


        $entitygenerator = $this->getDoctrine()->getManager();
        $iller = $entitygenerator->getRepository(Iller::class)->findAll();
        $il = new Iller();
        $il->setIlAdi("test");
        $il->setPlakaKodu(88);
        $il->setAlanKodu(20);
        $il->setBolge("karadeniz");
        $il->setNufus("199");
        $il->setYuzolcumu("55");
        $il->setErkekNufus("50");
        $il->setKadinNufus(60);
        $entitygenerator->persist($il);
        $entitygenerator->flush();
        return $this->render('anasayfa/index.html.twig', [
            'controller_name' => 'AnasayfaController',
            "iller"=>$iller
        ]);
    }
}
