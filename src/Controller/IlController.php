<?php

namespace App\Controller;

use App\Entity\Iller;
use phpDocumentor\Reflection\DocBlock\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class IlController extends AbstractController
{
    /**
     * @Route("/il/{il_id}", name="il")
     */
    public function index(string $il_id): Response
    {
       $il = $this->getDoctrine()->getRepository(Iller::class)->find($il_id);
        return $this->render('il/index.html.twig', [
            'controller_name' => 'IlController',
            'il'=>$il
        ]);
    }
    /**
     * @Route("api/iller",name="api_iller")
     */
    public function iller(){
        $iller = $this->getDoctrine()->getRepository(Iller::class)->find(407);
        return new JsonResponse();
    }
}
